'use strict';
module.exports = function (grunt) {
	require('time-grunt')(grunt);
	require('load-grunt-tasks')(grunt);

	grunt.initConfig(
		{
			yeoman: {
				app: 'app',
				dist: 'dist'
			},

			connect: {
				options: {
					port: 9000,
					// change this to '0.0.0.0' to access the server from outside
					hostname: 'localhost',
					livereload: 35729
				},
				livereload: {
					options: {
						open: true,
						base: [
							'<%= yeoman.dist %>'
						]
					}
				}
			},
			watch: {

				livereload: {
					options: {
						//https://github.com/gruntjs/grunt-contrib-watch/issues/186
						livereload: '<%= connect.options.livereload %>'
					},
					files: [
						'<%= yeoman.dist %>/*.html',
						'<%= yeoman.dist %>/scripts/**/*.js',
						'<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
					]
				}

			},
			htmlmin: {
				dist: {
					files: [
						{
							expand: true,
							cwd: '<%= yeoman.app %>',
							src: '*.html',
							dest: '<%= yeoman.dist %>'
						}
					]
				}
			},
			jshint: {
				options: {
					jshintrc: '.jshintrc'
				},
				gruntfile: {
					options: {
						jshintrc: '.jshintrc'
					},
					src: 'Gruntfile.js'
				}
			},

			clean: {
				dist: {
					files: [
						{
							dot: true,
							src: [
								'.tmp',
								'<%= yeoman.dist %>/*',
								'!<%= yeoman.dist %>/.git*'
							]
						}
					]
				}
			},

			copy: {
				dist: {
					"expand": true,
					"dot": true,
					cwd: '<%= yeoman.app %>',
					dest: '<%= yeoman.dist %>',
					"src": [
						"scripts/**/*"
					]
				}
			}
		}
	);

	grunt.registerTask(
		'default', ['clean', 'copy', 'buildChildren', 'htmlmin', 'connect:livereload', 'watch']
	);

	grunt.registerTask('buildChildren', 'Build child projects and copy in files', function () {
		var _ = require('lodash');
		var modules = grunt.file.readJSON('portal-modules.json');
		var copyTask = {};
		var hubTask = {};
		var tasks = [];
		for (var key in modules) {
			hubTask[key] = {};
			_.merge(hubTask[key], modules[key].hub);
			tasks.push('hub:'+key);

			copyTask[key] = {};
			_.merge(copyTask[key], modules[key].copy);
			copyTask[key].dest = '<%= yeoman.dist %>';
			tasks.push('copy:' + key);
		}
		grunt.config.set('copy', copyTask);
		grunt.config.set('hub', hubTask);
		console.log(grunt.config());
		grunt.registerTask('dynaimc', tasks);
		grunt.task.run('dynaimc');
	});

};
