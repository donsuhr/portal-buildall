# CMC Portal - Build All

Project to build cmc portal modules and copy their relevant parts into this projects `dist` folder.

**[Install](#markdown-header-install)**
 
**[Configure](#markdown-header-configure)**
 
**[Build](#markdown-header-build)**

**[Getting Started](#markdown-header-getting-started)**

---

## Install Node
Check to see if node is installed.

`node —version`

If not, download and install it. [http://nodejs.org/download/](http://nodejs.org/download/)

## Install Git
Check to see if git is installed.

`git —version`

If not, download and install it. [http://git-scm.com/downloads](http://git-scm.com/downloads)

## Install the build all project
Change directory to a working directory to serve as the <em>parent</em> of the portal-buildAll project. 

`git clone https://bitbucket.org/zorg128/portal-buildall.git`

then

`npm install`

---

##Configure

Configure `portal-modules.json` to specify which modules are built and copied.
```javascript
    "portal-core": {
		"hub": {
			"src": "../portal-core/GruntFile.js",
			"tasks": ["build"]
		},
		"copy": {
			"expand": true,
			"dot": true,
			"cwd": "../portal-core/dist",
			"src": [
				"scripts/common.noConfig.js",
				"scripts/vendor/**/*",
				"images/**/*",
				"styles/**/*",
				"bower_components/**/*"
			]
		}
	}
```

---

##Build
The default grunt task is configured to build the project. Issue the following command from the portal-buildall directory.

`grunt`

---

##Getting Started
To start working with the cmc portal, follow the steps below.

1. Install the generator by following the instructions [here](https://bitbucket.org/zorg128/generator-cmcportal).
1. Generate a project with the Yeoman generator installed in step one.
1. Develop the module.
1. Install the build-all project following the instructions above.
1. Configure the `portal-modules.json`. Create a section for each module you wish to have compiled.
1. The build-all project is built in a way as to expect any module placed anywhere on the page; There is no `main.js`. If you intend to preview the files with livePreview, the `index.html` file will need to be configured. Add a `<div>` and `require` or `define` for each module you would like to preview.
1. From the build-all project folder, issue the `grunt` command.
1. Copy the files from the `dist` folder to the production server.


Functionality common to multiple modules can be placed in the [portal-core](https://bitbucket.org/zorg128/portal-core) project. This project is installed as a bower dependency by the `generator-cmcPortal`. For instance, several modules use the kendo grid. The code from that module has been packaged into portal-core and is loaded by default.

After updating the portal-core project, modules using the code from portal-core should be updated `bower update` and re-tested `grunt test`.
