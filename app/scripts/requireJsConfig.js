/* jshint -W098:true */
var require = {
	baseUrl: 'scripts',
	paths: {
		'components': '../bower_components',
		'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min',
		'bootstrap': '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min',
		'kendo': '../bower_components/kendo-ui/js',
		'mockjax': '../bower_components/jquery-mockjax/jquery.mockjax',
		'mockjson': '../bower_components/mockJSON/js/jquery.mockjson',
		'placeholder': '../bower_components/jquery-placeholder/jquery.placeholder.min',
		'text': '../bower_components/requirejs-text/text'
	},
	deps: ['common.noConfig'],
	callback: function () {
		//console.log('core loaded');
	},
	//waitSeconds : 2,
	shim: {
		bootstrap: {
			deps: ['jquery']
		},
		jqueryMigrate: {
			deps: ['jquery']
		},
		placeholder: {
			deps: ['jquery']
		},
		'kendo/kendo.core.min': {
			deps: ['jquery']
		},
		mockjax: {
			deps: ['jquery']
		},
		mockjson: {
			deps: ['jquery']
		}

	}
};